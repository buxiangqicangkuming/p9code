package com.usian.pojo;

import lombok.Data;

@Data
public class User {
    private Integer uid;
    private String uname;
}
