package com.usian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootday03demo2Application {

    public static void main(String[] args) {
        SpringApplication.run(Springbootday03demo2Application.class, args);
    }

}
