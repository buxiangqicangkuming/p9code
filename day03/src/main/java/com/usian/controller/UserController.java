package com.usian.controller;

import com.usian.mapper.UserMapper;
import com.usian.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class UserController {
    @Autowired
    private UserMapper userMapper;

    @GetMapping("/findAll")
    @ResponseBody
    public List<User> findAll(String uname){
        List<User> list = userMapper.findAll(uname);
        return list;
    }
}
