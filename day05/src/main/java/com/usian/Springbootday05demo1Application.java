package com.usian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootday05demo1Application {

    public static void main(String[] args) {
        SpringApplication.run(Springbootday05demo1Application.class, args);
    }

}
