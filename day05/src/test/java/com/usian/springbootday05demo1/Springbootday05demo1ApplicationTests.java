package com.usian.springbootday05demo1;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.Mapper.UserMapper;
import com.usian.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SpringBootTest
class Springbootday05demo1ApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    void contextLoads() {
    }

    /**
     * 分页查询
     */
    @Test
    public void selectPageTest(){
        Page<User> page = new Page<>(1,2);
        userMapper.selectPage(page,null);
        System.out.println("当前页："+page.getCurrent() );
        System.out.println("页面大小："+page.getSize());
        System.out.println("总记录数："+ page.getTotal());
        System.out.println("总页数："+ page.getPages());
        System.out.println("当前页的数据："+ page.getRecords());
    }

    //以下是DQL

    @Test
    public void testAge(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.gt("age",18);
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println("集合列表："+ userList);
    }
    //

    @Test
    public void testLambdaAge(){
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.gt(User::getAge,18);
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println("集合列表："+ userList);
    }

    @Test
    public void testAndCondition(){
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.lt(User::getAge,30);
        queryWrapper.gt(User::getAge,10);
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println("集合列表："+ userList);
    }

    @Test
    public void testOrCondition(){
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.lt(User::getAge,10);
        queryWrapper.or();
        queryWrapper.gt(User::getAge,30);
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println("集合列表："+ userList);
    }

    @Test
    public void testIfNull(){
        Integer minAge = 10;
        Integer maxAge = 30;
        String name= "J";
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        if(minAge!=null){
            queryWrapper.gt(User::getAge,minAge);
        }
        if(maxAge!=null){
            queryWrapper.lt(User::getAge,maxAge);
        }
        if(name!=null){
            queryWrapper.like(User::getName,name);
        }
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println("用户列表："+userList);
    }

    @Test
    public void testIfNull2(){
        Integer minAge = 10;
        Integer maxAge = 30;
        String name= "J";
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.gt(minAge!=null,User::getAge,minAge);
        queryWrapper.lt(maxAge!=null,User::getAge,maxAge);
        queryWrapper.like(name!=null,User::getName,name);
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println("用户列表："+userList);
    }

    @Test
    void testSameColumn() {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.select(User::getId, User::getName, User::getAge);
        List<User> userList = userMapper.selectList(wrapper);
        System.out.println(userList);
    }

    @Test
    public void testGroup(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("gender,count(*)");
        queryWrapper.groupBy("gender");
        List<Map<String, Object>> maps = userMapper.selectMaps(queryWrapper);
        System.out.println("集合列表："+ maps);
    }

    @Test
    public void testOrder(){
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByDesc(User::getAge).last("limit 3");
        List<User> userList = userMapper.selectList(queryWrapper);
        System.out.println("用户列表："+userList);
    }

    //以下是DML

    @Test
    public void deletedml(){
        List<Integer> list = new ArrayList<>();
        list.add(3);
        userMapper.deleteBatchIds(list);
    }

    @Test
    public void selectdml(){
        List<Integer> list = new ArrayList<>();
        list.add(1);
        userMapper.selectBatchIds(list);
    }

    @Test
    void testLogicDeleted() {
        int row = userMapper.deleteById(5);
        System.out.println(row + "条记录被逻辑删除");
    }

    @Test
    public void testLock(){
        User user = userMapper.selectById(1);
        user.setAge(32);
        userMapper.updateById(user);
    }

    @Test
    public void testLock2(){
        User user1 = userMapper.selectById(6);
        User user2 = userMapper.selectById(6);
        user1.setName("张小 AA");
        userMapper.updateById(user1);
        user2.setName("张晓 BB");
        userMapper.updateById(user2);

    }

}
