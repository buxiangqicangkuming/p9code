package com.usian.day06;

import com.usian.mapper.UserMapper;
import com.usian.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class Day06ApplicationTests {

    @Autowired(required = false)
    private UserMapper userMapper;

    @Test
    public void testFindById() {
        List<User> userList = userMapper.selectList(null);
        System.out.println("用户对象"+userList);
    }

    @Test
    public void testInsert(){
        User user = new User();
        user.setName("老八");
        user.setGender("男");
        user.setPassword("root");
        user.setAge(20);
        user.setTel("351532123");
        userMapper.insert(user);
    }

    @Test
    public void testRemove(){
        userMapper.deleteById(8);
    }

    @Test
    public void testUpdate(){
        User user = new User();
        user.setId(6);
        user.setName("张小炮");
        userMapper.updateById(user);
    }
}
