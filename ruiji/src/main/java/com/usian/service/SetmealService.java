package com.usian.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.pojo.SetmealDto;

import java.util.List;

public interface SetmealService {
    void save(SetmealDto setmealDto);

    Page<SetmealDto> findByPage(Integer page, Integer pageSize, String name);

    void deleteByIds(List<Long> ids);

    void statusHandle(Integer id, List<Long> ids);
}
