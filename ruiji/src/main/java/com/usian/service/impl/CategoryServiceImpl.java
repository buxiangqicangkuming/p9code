package com.usian.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.config.CustomerException;
import com.usian.config.NameExistsException;
import com.usian.mapper.CategoryMapper;
import com.usian.mapper.DishMapper;
import com.usian.mapper.SetMealMapper;
import com.usian.pojo.Category;
import com.usian.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired(required = false)
    private DishMapper dishMapper;

    @Autowired(required = false)
    private SetMealMapper setMealMapper;

    @Override
    public void save(Category category) {
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Category::getName,category.getName());
        Category one = categoryMapper.selectOne(queryWrapper);
        if (one!=null){
            throw new NameExistsException(category.getName());
        }
        category.setCreateTime(LocalDateTime.now());
        category.setUpdateTime(LocalDateTime.now());
        categoryMapper.insert(category);
    }

    @Override
    public Page<Category> findByPage(Integer page, Integer pageSize) {
        Page<Category> pages=new Page<>(page,pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByAsc("sort");
        return categoryMapper.selectPage(pages,queryWrapper);
    }

    @Override
    public void deleteById(long id) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("category_id",id);
        Integer integer = dishMapper.selectCount(queryWrapper);
        if(integer>0){
            throw new CustomerException("该类别已经关联菜品不能直接删除");
        }
        Integer integer1 = setMealMapper.selectCount(queryWrapper);
        if(integer1>0){
            throw new CustomerException("该类别已经关联其他类别不能直接删除");
        }
        categoryMapper.deleteById(id);
    }

    @Override
    public void update(Category category) {
            category.setUpdateTime(LocalDateTime.now());
            LambdaUpdateWrapper<Category> updateWrapper=new LambdaUpdateWrapper<>();
            updateWrapper.set(Category::getName,category.getName());
            updateWrapper.set(Category::getSort,category.getSort());
            updateWrapper.set(Category::getUpdateTime,category.getUpdateTime());
            updateWrapper.set(Category::getUpdateUser,category.getUpdateUser());
            updateWrapper.eq(Category::getId,category.getId());
            categoryMapper.update(category,updateWrapper);
    }

    @Override
    public List<Category> findAll() {
        return categoryMapper.selectList(null);
    }

    @Override
    public List<Category> list(Integer type) {
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Category::getType,type);
        return categoryMapper.selectList(queryWrapper);
    }


}
