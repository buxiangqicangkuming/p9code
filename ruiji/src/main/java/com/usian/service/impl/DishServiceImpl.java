package com.usian.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.mapper.DishFlavorMapper;
import com.usian.mapper.DishMapper;
import com.usian.pojo.Dish;
import com.usian.pojo.DishDto;
import com.usian.pojo.DishFlavor;
import com.usian.service.DishService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class DishServiceImpl implements DishService {
    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private DishFlavorMapper dishFlavorMapper;



    @Transactional
    @Override
    public void save(DishDto dishDto) {
        dishDto.setStatus(1); // 1代表菜品启用  0代表下架商品
        dishDto.setSort(0);
        dishDto.setCreateTime(LocalDateTime.now());
        dishDto.setUpdateTime(LocalDateTime.now());
        dishMapper.insert(dishDto);
        //3.得到菜品的口味信息
        List<DishFlavor> flavorList = dishDto.getFlavors();
        //4. 给一个口味信息补全
        for (DishFlavor dishFlavor : flavorList) {
            dishFlavor.setDishId(dishDto.getId()); //口味对应的菜品的id
            dishFlavor.setCreateTime(dishDto.getCreateTime());
            dishFlavor.setUpdateTime(dishDto.getUpdateTime());
            dishFlavor.setUpdateUser(dishDto.getUpdateUser());
            dishFlavor.setCreateUser(dishDto.getCreateUser());
            dishFlavor.setIsDeleted(0);
            dishFlavorMapper.insert(dishFlavor);
        }
    }

    @Override
    public Page<DishDto> findByPage(Integer page, Integer pageSize, String name) {
        Page<DishDto> pageInfo=new Page<>(page,pageSize);
        List<DishDto> list=dishMapper.findAll(pageInfo,name);
        return pageInfo.setRecords(list);
    }

    @Override
    public DishDto findById(Long id) {
        Dish dish=dishMapper.findById(id);
        List<DishFlavor> dishFlavorList= dishFlavorMapper.findByDishId(id);
        DishDto dishDto = new DishDto();
        BeanUtils.copyProperties(dish,dishDto);
        dishDto.setFlavors(dishFlavorList);
        return dishDto;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateDish(DishDto dishDto) {
        dishDto.setUpdateTime(LocalDateTime.now());
        dishMapper.updateDish(dishDto);
        dishFlavorMapper.deleteByDishId(dishDto.getId());

        List<DishFlavor> flavorList=dishDto.getFlavors();
        for (DishFlavor dishFlavor : flavorList) {
            dishFlavor.setDishId(dishDto.getId());
            dishFlavor.setCreateTime(dishDto.getCreateTime());
            dishFlavor.setUpdateTime(dishDto.getUpdateTime());
            dishFlavor.setUpdateUser(dishDto.getUpdateUser());
            dishFlavor.setCreateUser(dishDto.getCreateUser());
            dishFlavor.setIsDeleted(0);
            dishFlavorMapper.insert(dishFlavor);
        }
    }

    @Override
    public List<Dish> list(Long categoryId) {
        List<Dish> dishList=dishMapper.findByCategoryId(categoryId);
        return dishList;
    }
}
