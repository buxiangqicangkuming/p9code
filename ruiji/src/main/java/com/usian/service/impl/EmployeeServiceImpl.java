package com.usian.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.config.NameExistsException;
import com.usian.mapper.EmployeeMapper;
import com.usian.pojo.Employee;
import com.usian.pojo.Result;
import com.usian.service.EmployeeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;


import java.time.LocalDateTime;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired(required = false)
    private EmployeeMapper employeeMapper;
    @Override
    public Result<Employee> login(Employee employee) {
        Employee dbEmployee = employeeMapper.login(employee);
        if(dbEmployee==null){
            return Result.error("用户名不存在");
        }
        String inputPassword = DigestUtils.md5DigestAsHex(employee.getPassword().getBytes());
        if(!dbEmployee.getPassword().equals(inputPassword)){
            return Result.error("密码错误");
        }
        if(dbEmployee.getStatus()==0){
            return  Result.error("禁用");
        }
        return Result.success(dbEmployee);
    }

    @Override
    public void save(Employee employee) {
        Employee emp=employeeMapper.login(employee);
        if (emp!=null){
            throw new NameExistsException("用户名已存在，换");
        }
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
        employee.setStatus(1);
        employee.setCreateTime(LocalDateTime.now());
        employee.setUpdateTime(LocalDateTime.now());
        employeeMapper.insert(employee);
    }

    @Override
    public Page<Employee> page(Integer page, Integer pageSize, String name) {
        Page<Employee> page1=new Page<>(page,pageSize);
        LambdaQueryWrapper<Employee> lambdaQueryWrapper=new LambdaQueryWrapper();
        lambdaQueryWrapper.like(StringUtils.isNotEmpty(name),Employee::getUsername,name);
        Page<Employee> employeePage=employeeMapper.selectPage(page1,lambdaQueryWrapper);
        return employeePage;
    }

    @Override
    public void updateEmp(Employee employee) {
        employee.setUpdateTime(LocalDateTime.now());
        LambdaUpdateWrapper<Employee> updateWrapper=new LambdaUpdateWrapper<>();
        if(employee.getUsername()!=null && employee.getUsername()!=""){
            updateWrapper.set(Employee::getUsername,employee.getUsername());
        }
        if(employee.getName()!=null && employee.getName()!=""){
            updateWrapper.set(Employee::getName,employee.getName());
        }
        if(employee.getPassword()!=null && employee.getPassword()!=""){
            updateWrapper.set(Employee::getPassword,employee.getPassword());
        }
        if(employee.getPhone()!=null && employee.getPhone()!=""){
            updateWrapper.set(Employee::getPhone,employee.getPhone());
        }
        if(employee.getSex()!=null && employee.getSex()!=""){
            updateWrapper.set(Employee::getSex,employee.getSex());
        }
        if(employee.getIdNumber()!=null && employee.getIdNumber()!=""){
            updateWrapper.set(Employee::getIdNumber,employee.getIdNumber());
        }
        if(employee.getStatus()!=null){
            updateWrapper.set(Employee::getStatus,employee.getStatus());
        }
        if(employee.getUpdateTime()!=null ){
            updateWrapper.set(Employee::getUpdateTime,employee.getUpdateTime());
        }
        if(employee.getUpdateUser()!=null ){
            updateWrapper.set(Employee::getUpdateUser,employee.getUpdateUser());
        }
        updateWrapper.eq(Employee::getId,employee.getId());
        employeeMapper.update(employee,updateWrapper);
    }

    @Override
    public Employee findById(Long id) {
        return employeeMapper.selectById(id);
    }


}
