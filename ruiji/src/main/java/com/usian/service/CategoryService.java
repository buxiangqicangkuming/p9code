package com.usian.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.pojo.Category;

import java.util.List;

public interface CategoryService {
    void save(Category category);

    Page<Category> findByPage(Integer page, Integer pageSize);

    void deleteById(long id);

    void update(Category category);

    List<Category> findAll();

    List<Category> list(Integer type);
}
