package com.usian.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.pojo.Dish;
import com.usian.pojo.DishDto;

import java.util.List;

public interface DishService {
    void save(DishDto dishDto);

    Page<DishDto> findByPage(Integer page, Integer pageSize, String name);

    DishDto findById(Long id);

    void updateDish(DishDto dishDto);

    List<Dish> list(Long categoryId);
}
