package com.usian.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.pojo.Employee;
import com.usian.pojo.Result;

public interface EmployeeService {
    Result<Employee> login(Employee employee);

    void save(Employee employee);

    Page<Employee> page(Integer page, Integer pageSize, String name);




    void updateEmp(Employee employee);

    Employee findById(Long id);
}
