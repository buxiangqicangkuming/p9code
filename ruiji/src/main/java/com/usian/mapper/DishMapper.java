package com.usian.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.pojo.Dish;
import com.usian.pojo.DishDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface DishMapper extends BaseMapper<Dish> {
    List<DishDto> findAll(@Param("pageInfo") Page<DishDto> pageInfo,@Param("name") String name);

    @Select("select * from dish where id=#{id}")
    Dish findById(Long id);

    void updateDish(DishDto dishDto);

    @Select("select * from dish where category_id = #{categoryId}")
    List<Dish> findByCategoryId(Long categoryId);

}
