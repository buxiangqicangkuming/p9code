package com.usian.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.usian.pojo.DishFlavor;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface DishFlavorMapper extends BaseMapper<DishFlavor> {
    @Select("select * from dish_flavor where dish_id=#{id}")
    List<DishFlavor> findByDishId(Long id);

    @Delete("delete from dish_flavor where dish_id=#{id}")
    void deleteByDishId(Long id);
}
