package com.usian.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.usian.pojo.Category;

public interface CategoryMapper extends BaseMapper<Category> {
}
