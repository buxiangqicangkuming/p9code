package com.usian.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.pojo.Setmeal;
import com.usian.pojo.SetmealDto;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface SetMealMapper extends BaseMapper<Setmeal> {
    List<SetmealDto> findAll(Page<SetmealDto> page1, String name);

    @Update("update setmeal set status=#{id} where id=#{aa}")
    void updateStatus(Integer id, Long aa);
}
