package com.usian.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.usian.pojo.SetmealDish;
import org.apache.ibatis.annotations.Delete;

public interface SetmealDishMapper extends BaseMapper<SetmealDish> {
    @Delete("delete from setmeal_dish where setmeal_id=#{id}")
    void deleteBySetmealId(Long id);
}
