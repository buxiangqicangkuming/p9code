package com.usian.config;

public class NameExistsException extends RuntimeException{
    public NameExistsException(String message) {
        super(message);
    }
}
