package com.usian.config;

import com.usian.pojo.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
    /*
   @ExceptionHandler : 用于指定这个方法处理那些类型的异常的。

    */
    @ExceptionHandler(NameExistsException.class)
    public Result exceptionHandler(NameExistsException e){
        e.printStackTrace(); //打印异常信息，否则控制台没有任何的内容
        return Result.error(e.getMessage()+"已经存在");
    }

    /*
    @ExceptionHandler : 用于指定这个方法处理那些类型的异常的。

     */
    @ExceptionHandler(Exception.class)
    public Result exceptionHandler(Exception e){
        e.printStackTrace(); //打印异常信息，否则控制台没有任何的内容
        return Result.error("目前访问人数过多，请稍后！");
    }

    @ExceptionHandler(CustomerException.class)
    public Result exceptionHandler(CustomerException e){
        e.printStackTrace(); //打印异常信息，否则控制台没有任何的内容
        return Result.error(e.getMessage());
    }
}
