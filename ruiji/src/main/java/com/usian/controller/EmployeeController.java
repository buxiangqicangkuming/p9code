package com.usian.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.pojo.Employee;
import com.usian.pojo.Result;
import com.usian.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;
    @PostMapping("/login")
    public Result login(@RequestBody Employee employee, HttpSession session){
        Result<Employee> result = employeeService.login(employee);
        if(result.getCode()==1){
            session.setAttribute("employee",result.getData().getId());
        }
        return result;
    }
    @PostMapping("/logout")
    public Result logout(HttpSession session){
        session.invalidate();
        return Result.success("退出登录");
    }
    @PostMapping
    public Result save(@RequestBody Employee employee,HttpSession session){
        Long empId= (Long) session.getAttribute("employee");
        employee.setCreateUser(empId);
        try {
            employee.setUpdateUser(empId);
        } catch (Exception e) {
            throw new RuntimeException("设置修改人失败");
        }
        employeeService.save(employee);
        return Result.success("添加");


    }
    @GetMapping("/page")
    public Result page(@RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "3") Integer pageSize,String name){
        Page<Employee> page1=employeeService.page(page,pageSize,name);
        return Result.success(page1);
    }
    @PutMapping
    public Result  updateEmp(@RequestBody Employee employee,HttpSession session){
        //补全修改人
        //1.先从session中取出当前登录的用户
        Long empId = (Long) session.getAttribute("employee");
        //2. 补全员工的创建人与修改人
        employee.setUpdateUser(empId);
        employeeService.updateEmp(employee);
        return Result.success("修改成功");
    }
    @GetMapping("{id}")
    public Result findById(@PathVariable Long id){
        Employee employee=employeeService.findById(id);
        return Result.success(employee);
    }

}
