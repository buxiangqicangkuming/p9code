package com.usian.controller;

import com.usian.pojo.Result;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/common")
public class CommonController {
    @Value("${reggie.path}")
    private String path;

    @PostMapping("/upload")
    public Result<String> upload(MultipartFile file) throws IOException {
        String filename = file.getOriginalFilename();
        String extName = filename.substring(filename.lastIndexOf("."));
        String uuidFileName = UUID.randomUUID().toString() + extName;
        File dir = new File(path);
        if (!dir.exists()){
            dir.mkdirs();
        }
        File targetFile = new File(dir, uuidFileName);
        file.transferTo(targetFile);
        return Result.success(uuidFileName);
    }

    @GetMapping("/download")
    public void download(String name, HttpServletResponse response) throws IOException {
        File targetFile = new File(path, name);
        FileInputStream fileInputStream = new FileInputStream(targetFile);
        ServletOutputStream outputStream = response.getOutputStream();
        byte[] buf=new byte[1024];
        int length=0;
        while ((length=fileInputStream.read(buf))!=-1) {
            outputStream.write(buf,0,length);
        }
        fileInputStream.close();
    }
}
