package com.usian.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.pojo.Result;
import com.usian.pojo.SetmealDto;
import com.usian.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/setmeal")
public class SetmealController {

    @Autowired
    private SetmealService setmealService;

    @PostMapping
    public Result save(@RequestBody SetmealDto setmealDto, HttpSession session){
        Long empId= (Long) session.getAttribute("employee");
        setmealDto.setCreateUser(empId);
        setmealDto.setUpdateUser(empId);
        setmealService.save(setmealDto);
        return Result.success("添加套餐成功");
    }

    @GetMapping("/page")
    public Result page(@RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "10")Integer pageSize,String name){
        Page<SetmealDto> pageResult=setmealService.findByPage(page,pageSize,name);
        return Result.success(pageResult);
    }

    @DeleteMapping
    public Result deleteByIds(@RequestParam List<Long> ids){
        setmealService.deleteByIds(ids);
        return Result.success("删除成功");
    }

    @PostMapping("status/{id}")
    public Result statusHandle(@PathVariable Integer id,@RequestParam List<Long> ids){
        setmealService.statusHandle(id,ids);
        return Result.success("修改成功");
    }
}
