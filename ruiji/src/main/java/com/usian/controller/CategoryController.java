package com.usian.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.pojo.Category;
import com.usian.pojo.Result;
import com.usian.service.CategoryService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @PostMapping
    public Result save(@RequestBody Category category, HttpSession session){
        Long empId = (Long) session.getAttribute("employee");
        category.setCreateUser(empId);
        category.setUpdateUser(empId);
        categoryService.save(category);
        return Result.success("添加类别成功");
    }

    @GetMapping("/page")
    public Result page(@RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "10") Integer pageSize){
        Page<Category> pageResult = categoryService.findByPage(page,pageSize);
        return Result.success(pageResult);
    }

    @DeleteMapping
    public Result page(long id){
        categoryService.deleteById(id);
        return Result.success("删除类别成功");
    }

    @PutMapping
    public Result update(@RequestBody Category category,HttpSession session){
        Long empId = (Long) session.getAttribute("employee");
        category.setUpdateUser(empId);
        categoryService.update(category);
        return Result.success("修改类别成功");
    }

    @GetMapping("/exportExcel")
    public void exportExcel(HttpServletResponse response) throws IOException {
        String[] handers={"类型名称","类型名字","排序","创建时间","修改时间"};
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("分类信息");
        Row row = sheet.createRow(0);
        for (int i = 0; i < handers.length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(handers[i]);
        }
        List<Category> categoryList= categoryService.findAll();
        for (int i = 0; i < categoryList.size(); i++) {
            Category category = categoryList.get(i);
            row= sheet.createRow(i+1);
            row.createCell(0).setCellValue(category.getType());
            row.createCell(1).setCellValue(category.getName());
            row.createCell(2).setCellValue(category.getSort());
            DateTimeFormatter dateTimeFormatter=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            row.createCell(3).setCellValue(dateTimeFormatter.format(category.getCreateTime()));
            row.createCell(4).setCellValue(dateTimeFormatter.format(category.getUpdateTime()));
        }
        response.setHeader("content-disposition","attachment;filename=category.xlsx");
        workbook.write(response.getOutputStream());
    }

    @GetMapping("/list")
    public Result list(Integer type){
        List<Category> categoryList= categoryService.list(type);
        return Result.success(categoryList);
    }
}
