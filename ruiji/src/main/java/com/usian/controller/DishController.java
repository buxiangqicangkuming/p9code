package com.usian.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.mapper.DishMapper;
import com.usian.pojo.Dish;
import com.usian.pojo.DishDto;
import com.usian.pojo.Result;
import com.usian.service.DishFlavorService;
import com.usian.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/dish")
public class DishController {

    @Autowired
    private DishService dishService;

    @Autowired
    private DishFlavorService dishFlavorService;

    @PostMapping
    public Result save(@RequestBody DishDto dishDto, HttpSession session){
        Long empId = (Long) session.getAttribute("employee");
        dishDto.setCreateUser(empId);
        dishDto.setUpdateUser(empId);
        dishService.save(dishDto);
        return Result.success("添加成功");
    }

    @GetMapping("/page")
    public Result page(@RequestParam(defaultValue = "1")Integer page,@RequestParam(defaultValue = "10")Integer pageSize,String name){
        Page<DishDto> pageResult= dishService.findByPage(page,pageSize,name);
        return Result.success(pageResult);
    }

    @GetMapping("/{id}")
    public Result findById(@PathVariable Long id){
        DishDto dishDto= dishService.findById(id);
        return Result.success(dishDto);
    }

    @PutMapping
    public Result updateDish(@RequestBody DishDto dishDto,HttpSession session){
        Long empId=(Long) session.getAttribute("employee");
        dishDto.setUpdateUser(empId);
        dishService.updateDish(dishDto);
        return Result.success("修改菜品成功");
    }

    @GetMapping("/list")
    public Result list(Long categoryId){
        List<Dish> dishList=dishService.list(categoryId);
        return Result.success(dishList);
    }
}
