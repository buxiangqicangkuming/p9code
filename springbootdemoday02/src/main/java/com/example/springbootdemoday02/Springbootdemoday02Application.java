package com.example.springbootdemoday02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootdemoday02Application {

    public static void main(String[] args) {
        SpringApplication.run(Springbootdemoday02Application.class, args);
    }

}
