package com.usian.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.usian.pojo.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {

    @Select("select * from employee e where e.username=#{username}")
    Employee findByName(Employee employee);

}
