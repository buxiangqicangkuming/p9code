package com.usian.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.common.Result;
import com.usian.config.NameExistsException;
import com.usian.mapper.EmployeeMapper;
import com.usian.pojo.Employee;
import com.usian.service.EmployeeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public Result<Employee> login(Employee employee) {
        Employee dbEmployee = employeeMapper.findByName(employee);
        if(dbEmployee==null){
            return Result.error("用户名不存在");
        }
        String inputPassword = DigestUtils.md5DigestAsHex(employee.getPassword().getBytes());
        if(!dbEmployee.getPassword().equals(inputPassword)){
            return Result.error("密码错误");
        }
        if(dbEmployee.getStatus()==0){
            return  Result.error("用户被禁用");
        }
        return Result.success(dbEmployee);
    }

    @Override
    public void save(Employee employee) {
        Employee dbEmp = employeeMapper.findByName(employee);
        if (dbEmp!=null){
            throw new NameExistsException(employee.getUsername());
        }
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
        employee.setStatus(1);
        employee.setCreateTime(LocalDateTime.now());
        employee.setUpdateTime(LocalDateTime.now());
        employeeMapper.insert(employee);
    }

    @Override
    public Page<Employee> findPage(Integer page, Integer pageSize, String name) {
        Page<Employee> page1=new Page<>(page,pageSize);
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name),Employee::getName,name);
        Page<Employee> page2 = employeeMapper.selectPage(page1, queryWrapper);
        return page2;
    }

    @Override
    public void updateEmp(Employee employee) {
        employee.setUpdateTime(LocalDateTime.now());
        LambdaUpdateWrapper<Employee> updateWrapper = new LambdaUpdateWrapper<>();
        if (employee.getUsername()!=null&&employee.getUsername()!="null"){
            updateWrapper.set(Employee::getUsername,employee.getUsername());
        }
        if (employee.getName()!=null&&employee.getName()!="null"){
            updateWrapper.set(Employee::getName,employee.getName());
        }
        if (employee.getPassword()!=null&&employee.getPassword()!="null"){
            updateWrapper.set(Employee::getPassword,employee.getPassword());
        }
        if (employee.getPhone()!=null&&employee.getPhone()!="null"){
            updateWrapper.set(Employee::getPhone,employee.getPhone());
        }
        if (employee.getSex()!=null&&employee.getSex()!="null"){
            updateWrapper.set(Employee::getSex,employee.getSex());
        }
        if (employee.getIdNumber()!=null&&employee.getIdNumber()!="null"){
            updateWrapper.set(Employee::getIdNumber,employee.getIdNumber());
        }
        if (employee.getStatus()!=null){
            updateWrapper.set(Employee::getStatus,employee.getStatus());
        }
        if (employee.getUpdateTime()!=null){
            updateWrapper.set(Employee::getUpdateTime,employee.getUpdateTime());
        }
        if (employee.getUpdateUser()!=null){
            updateWrapper.set(Employee::getUpdateUser,employee.getUpdateUser());
        }
        updateWrapper.eq(Employee::getId,employee.getId());
        employeeMapper.update(employee,updateWrapper);
    }

    @Override
    public Employee findById(long id) {
        Employee employee= employeeMapper.selectById(id);
        return employee;
    }


}
