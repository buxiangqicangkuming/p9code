package com.usian.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.usian.common.Result;
import com.usian.pojo.Employee;

public interface EmployeeService {
    Result<Employee> login(Employee employee);

    void save(Employee employee);

    Page<Employee> findPage(Integer page, Integer pageSize, String name);

    void updateEmp(Employee employee);

    Employee findById(long id);
}
