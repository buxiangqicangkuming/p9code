package cn.jiyun.service.impl;

import cn.jiyun.mapper.UserMapper;
import cn.jiyun.pojo.User;
import cn.jiyun.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public List<User> findAll() {
        return userMapper.selectList(null);
    }

    @Override
    public User findUser(User user) {
        return userMapper.findUser(user);
    }

    @Override
    public void addUser(User user) {
        user.setBirth(new Date());
        userMapper.insert(user);
    }

    @Override
    public void del(Integer id) {
        userMapper.deleteById(id);
    }

    @Override
    public User toedit(Integer id) {
        return userMapper.selectById(id);
    }

    @Override
    public void edit(User user) {
        user.setBirth(new Date());
        userMapper.updateById(user);
    }


}
