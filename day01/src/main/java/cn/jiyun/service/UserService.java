package cn.jiyun.service;

import cn.jiyun.pojo.User;

import java.util.List;

public interface UserService {
    List<User> findAll();

    User findUser(User user);

    void addUser(User user);

    void del(Integer id);

    User toedit(Integer id);

    void edit(User user);
}
