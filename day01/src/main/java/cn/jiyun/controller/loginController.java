package cn.jiyun.controller;

import cn.jiyun.pojo.User;
import cn.jiyun.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/login")
public class loginController {

    @Autowired
    private UserService userService;
    @RequestMapping("/login")
    public String login(User user, HttpSession session){
        User user1=userService.findUser(user);
        if (user1!=null){
            session.setAttribute("user",user1);
            return "redirect:/user/findAll";
        }
        return "login";
    }
}
