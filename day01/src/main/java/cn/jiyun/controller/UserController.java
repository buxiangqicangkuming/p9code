package cn.jiyun.controller;

import cn.jiyun.pojo.User;
import cn.jiyun.service.UserService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/findAll")
    public String findAll(Model model){
        List<User> list=this.userService.findAll();
        model.addAttribute("list",list);
        return "list";
    }

    @RequestMapping("/toadd")
    public String toadd(){
        return "add";
    }

    @RequestMapping("/addUser")
    public String addUser(User user){
     userService.addUser(user);
        return "redirect:/user/findAll";
    }

    @RequestMapping("/del")
    public String del(Integer id){
        userService.del(id);
        return "redirect:/user/findAll";
    }

    @RequestMapping("/toedit")
    public String toedit(Model model,Integer id){
        User user=userService.toedit(id); //回显
        model.addAttribute("user",user);
        return "edit";
    }

    @RequestMapping("/edit")
    public String edit(User user){
        userService.edit(user);
        return "redirect:findAll";
    }
}
