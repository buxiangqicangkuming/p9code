package com.usian.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class Order {
    private Long id;
    @TableField(value = "user_id")
    private Long userId;
    private String name;
    private String price;
    private Integer num;
    private User user;

}
