package com.usian.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.usian.entity.Order;
import org.apache.ibatis.annotations.Select;

public interface OrderDao {
    @Select("select * from tb_order where id=#{id}")
    Order selectById(Long id);
}
