package com.usian.service;

import com.usian.entity.Order;

public interface OrderService {
    Order findById(Long id);
}
