package com.usian.service.Impl;

import com.usian.dao.OrderDao;
import com.usian.entity.Order;
import com.usian.entity.User;
import com.usian.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderDao orderDao;
    @Autowired
    RestTemplate restTemplate;
    @Override
    public Order findById(Long id) {
       Order order = orderDao.selectById(id);
       User user = restTemplate.getForObject("http://userservice/user/"+order.getUserId(), User.class);
       order.setUser(user);
       return order;
    }
}
