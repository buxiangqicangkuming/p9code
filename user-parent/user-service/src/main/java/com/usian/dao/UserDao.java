package com.usian.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.usian.entity.User;

public interface UserDao extends BaseMapper<User> {
}
