package com.usian.service;

import com.usian.entity.User;

public interface UserService {
    User findById(Long id);
}
