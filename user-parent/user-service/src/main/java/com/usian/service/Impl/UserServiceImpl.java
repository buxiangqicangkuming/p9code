package com.usian.service.Impl;

import com.usian.dao.UserDao;
import com.usian.entity.User;
import com.usian.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Override
    public User findById(Long id) {
        return userDao.selectById(id);
    }
}
