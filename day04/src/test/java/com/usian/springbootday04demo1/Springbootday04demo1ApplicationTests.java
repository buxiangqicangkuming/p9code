package com.usian.springbootday04demo1;

import com.usian.mapper.UserMapper;
import com.usian.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class Springbootday04demo1ApplicationTests {

    @Autowired(required = false)
    private UserMapper userMapper;

    @Test
    public void testFindById() {
        List<User> userList = userMapper.selectList(null);
        System.out.println("用户对象:"+userList);
    }

    @Test
    public void Insert(){
        User user = new User();
        user.setId(3);
        user.setUname("王五");
        user.setAge("19");
        userMapper.insert(user);
    }

    @Test
    public void  Delete(){
        userMapper.deleteById(3);
    }


    @Test
    public void Update(){
        User user = new User();
        user.setId(2);
        user.setUname("小排");
        userMapper.updateById(user);
    }
}
