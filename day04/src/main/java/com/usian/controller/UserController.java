package com.usian.controller;


import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @GetMapping("/hello")
    public String hello(Model model){
        model.addAttribute("msg","hello thymeleaf");
        return "msg";
    }
}
